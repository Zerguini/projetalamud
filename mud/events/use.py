# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class UseEvent(Event2):
    NAME = "use"

    def perform(self):
        if not self.object.has_prop("usable"):
            self.fail()
            return self.inform("use.failed")
        self.inform("use")
